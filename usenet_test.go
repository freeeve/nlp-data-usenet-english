package usenet

import (
	"testing"

	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type UsenetSuite struct{}

var _ = Suite(&UsenetSuite{})

func (s *UsenetSuite) TestProbTrue(c *C) {
	p := Prob("what is the white house")
	c.Assert(p > 0.0, Equals, true)
}

func (s *UsenetSuite) TestProbFalse(c *C) {
	p := Prob("horse stapler battery")
	c.Assert(p > 0.0, Equals, false)
}

func (s *UsenetSuite) TestProbTrue2(c *C) {
	p := Prob("Some students like to study in the mornings")
	c.Assert(p > 0.0, Equals, true)
}
