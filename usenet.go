package usenet

import (
	"bytes"
	"compress/gzip"
	"encoding/gob"
	"log"
	"math"
	"strings"

	"github.com/wfreeman/nlp/util"
)

var freqtrie map[string]map[string]float32

func init() {
	triedata, err := Asset("data/westbury.2-gram.gob.gz")
	if err != nil {
		log.Fatal(err)
	}
	gzr, err := gzip.NewReader(bytes.NewReader(triedata))
	if err != nil {
		log.Fatal("error creating gzip reader", err)
	}
	err = gob.NewDecoder(gzr).Decode(&freqtrie)
	if err != nil {
		log.Fatal("error loading usenet trie data", err)
	}
}

func Prob(s string) float64 {
	sp := strings.Split(s, " ")
	ngs := util.Ngrams(sp, 2)
	p := float64(0)
	for _, ng := range ngs {
		ng[0] = strings.ToLower(strings.TrimSpace(ng[0]))
		ng[1] = strings.ToLower(strings.TrimSpace(ng[1]))
		curp, ok := freqtrie[ng[0]][ng[1]]
		if !ok {
			curp = float32(0.0)
		}
		pp := -math.Log(float64(curp))
		p += pp
		//fmt.Println(p, curp, ng)
	}
	return math.Exp(-p)
}

func ProbLoose(s string) float64 {
	sp := strings.Split(s, " ")
	ngs := util.Ngrams(sp, 2)
	p := float64(0)
	for _, ng := range ngs {
		ng[0] = strings.ToLower(strings.TrimSpace(ng[0]))
		ng[1] = strings.ToLower(strings.TrimSpace(ng[1]))
		curp, ok := freqtrie[ng[0]][ng[1]]
		if !ok {
			curp = float32(0.0)
		}
		pp := float64(curp)
		p += pp
		//fmt.Println(p, curp, ng)
	}
	return p
}
