package main

import (
	"bufio"
	"compress/bzip2"
	"compress/gzip"
	"encoding/gob"
	"fmt"
	"log"
	"math"
	"os"
	"strings"

	"github.com/wfreeman/nlp/util"
)

var (
	filename = ""
	outname  = ""
	trie     = map[string]map[string]uint32{}
)

func init() {
	filename = "westbury.txt.bz2"
	outname = "westbury.2-gram.freq.trie.gob.gz"
}

func main() {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal("unable to open file", filename)
	}
	bzr := bzip2.NewReader(f)

	b := bufio.NewReader(bzr)
	c := 0

	out, err := os.Create(outname)
	if err != nil {
		log.Fatal("unable to create file", outname)
	}
	zout := gzip.NewWriter(out)
	enc := gob.NewEncoder(zout)
	for {
		line, err := b.ReadString(byte('\n'))
		if err != nil {
			log.Println(err)
			break
		}
		sp := strings.Split(line, " ")
		ngs := util.Ngrams(sp, 2)
		if len(ngs) > 0 {
			for _, ng := range ngs {
				ng[0] = strings.ToLower(strings.TrimSpace(ng[0]))
				ng[1] = strings.ToLower(strings.TrimSpace(ng[1]))
				if len(ng[0]) > 20 || len(ng[1]) > 20 || len(ng[1]) == 0 || len(ng[0]) == 0 {
					break
				}
				m := trie[ng[0]]
				if m == nil {
					m = make(map[string]uint32)
					trie[ng[0]] = m
				}
				x := m[ng[1]]
				if x == 100000 {
					fmt.Println(ng, "count:", c, "1-grams:", len(trie), "2-grams:", len(m))
				}
				if x < math.MaxUint32 {
					m[ng[1]] = x + 1
				}
			}
			if c%1000000 == 0 {
				fmt.Println("pruning, lc:", c, ", 1-grams:", len(trie))
				prune()
				fmt.Println("after pruning, lc:", c, ", 1-grams:", len(trie))
			}
			c++
		}
	}
	prune()
	enc.Encode(trie)
	zout.Close()
}

func prune() {
	for n1, m := range trie {
		for n2, v := range m {
			if v < 5 {
				delete(m, n2)
			}
		}
		if len(m) == 0 {
			delete(trie, n1)
		}
	}
}
