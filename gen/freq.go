package main

import (
	"compress/gzip"
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"regexp"
)

var (
	inname  = ""
	outname = ""
	trie    = map[string]map[string]uint32{}
	outtrie = map[string]map[string]float32{}
)

func init() {
	inname = "westbury.2-gram.freq.trie.gob.gz"
	outname = "westbury.2-gram.gob.gz"
}

func main() {
	f, err := os.Open(inname)
	if err != nil {
		log.Fatal("unable to open file", inname)
	}
	gzr, err := gzip.NewReader(f)
	if err != nil {
		log.Fatal("unable to create zip reader")
	}
	err = gob.NewDecoder(gzr).Decode(&trie)
	if err != nil {
		log.Fatal("unable to decode trie", err)
	}

	out, err := os.Create(outname)
	if err != nil {
		log.Fatal("unable to create file", outname)
	}
	zout := gzip.NewWriter(out)
	enc := gob.NewEncoder(zout)
	total := uint64(0)

	var wordish = regexp.MustCompile(`[a-z\-\.\,]+`)
	for n1, m := range trie {
		if !wordish.MatchString(n1) {
			delete(trie, n1)
			fmt.Println("1deleting", n1)
			continue
		}
		for n2, _ := range m {
			if !wordish.MatchString(n2) {
				fmt.Println("1deleting", n2)
				delete(m, n2)
			}
		}
	}
	prune()

	for _, m := range trie {
		for _, freq := range m {
			total += uint64(freq)
		}
	}
	fmt.Println("total: ", total)

	for n1, m := range trie {
		outtrie[n1] = make(map[string]float32, len(m))
		n1total := uint64(0)
		for _, freq := range m {
			n1total += uint64(freq)
		}
		for n2, freq := range m {
			outtrie[n1][n2] = float32(float64(freq) / float64(n1total))
			fmt.Println(n1, n2, outtrie[n1][n2])
		}
	}
	enc.Encode(outtrie)
	zout.Close()
}

func prune() {
	for n1, m := range trie {
		if len(m) == 0 {
			delete(trie, n1)
		}
	}
}
